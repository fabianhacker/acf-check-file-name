If you use an ACF upload field and want to check the name of your uploaded file for special characters, you can use this code.

Just copy the code from code.php and put it in your functions.php. After that change the field_key with the right one from your installation.

Enjoy!

If you find issues please let me know