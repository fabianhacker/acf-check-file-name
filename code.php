function check_file_name($errors, $file, $attachment, $field) { 

    $filename = $attachment['url']; 
    $start = strrpos($filename, 'fakepath', -1);
    $end = strrpos($filename, '.', -1); 
    $length = $end - $start - 9; 
    
    $filename = substr($filename, $start + 9, $length); 
    
    if(preg_match('/[^[a-zA-Z0-9\s-]+$/', $filename)) { 
        $errors[] = 'Errormessage'; 
    } 
    
    return $errors; 
} 
add_filter('acf/validate_attachment/key=field_123456789', 'check_file_name', 10, 4);